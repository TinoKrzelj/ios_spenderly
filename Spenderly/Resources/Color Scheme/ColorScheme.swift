//
//  ColorScheme.swift
//  Spenderly
//
//  Created by Tino Krzelj on 26/05/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

struct ColorScheme {
    static var background: UIColor { return UIColor(red:0.15, green:0.20, blue:0.22, alpha:1.0) }
    
    static var backgroundGrey: UIColor { return UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1.00) }
    static var backgroundLightBlack: UIColor { return UIColor.black.withAlphaComponent(0.025) }
    static var darkGrey: UIColor { return UIColor.darkGray }
    static var alertGrey: UIColor { return UIColor(red:0.85, green:0.85, blue:0.85, alpha:1.0) }
    static var whiteText: UIColor { return UIColor.white }
    static var separatorGrey: UIColor { return UIColor(red: 0.88, green: 0.88, blue: 0.88, alpha: 1.0) }
    static var clearColor: UIColor { return UIColor.clear }
    static var incomeGreen: UIColor { return UIColor(red:0.40, green:0.67, blue:0.55, alpha:1.0) }
    static var expenseRed: UIColor { return UIColor(red:0.96, green:0.20, blue:0.25, alpha:1.0) }
    static var alertRed: UIColor { return UIColor(red:0.90, green:0.22, blue:0.21, alpha:1.0) }

}
