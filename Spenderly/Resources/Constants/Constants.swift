//
//  Constants.swift
//  Spenderly
//
//  Created by Tino Krzelj on 26/05/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import Foundation

// MARK: - General

var preferedDateFormat = "d MMMM YYYY"


// MARK: - Storyboard ids


struct StoryboardIds {
    static let viewController = "ViewController"
    static let currencyViewController = "CurrencyViewController"
    static let mainNavigationController = "MainNavigationController"
}

struct UserDefaultsKeys {
    static let applicationFirstLaunch = "applicationFirstLaunch"
    static let currentCurrency = "currentCurrency"
    static let currentIncomeValue = "currentIncomeValue"
    static let currentExpenseValue = "currentExpenseValue"
    static let currentOverallBalance = "currentOverallBalance"
    static let items = "items"
}

// MARK: - Nib keys


struct NibKey {
    static var mainNavigationBar = "MainNavigationBar"
    static var mainInfoArea = "MainInfoArea"
    static var listInfoArea = "ListInfoArea"
    static var listInfoCell = "ListInfoCell"
    static var keyboardArea = "KeyboardArea"
    static var detailsArea = "DetailsArea"
    static var optionButtonsArea = "OptionButtonsArea"
    static var categoryCell = "CategoryCell"
    static var settingsCell = "SettingCell"
}


// MARK: - Fonts

struct FontNames {
    static var avenirNextMedium = "Avenir Next"
    static var avenirNextRegular = "Avenir Next"
}

// MARK: - Segues


struct CustomSegues {
    static var showInputViewController = "showInputViewController"
    static var showNoteViewController = "showNoteViewController"
    static var showDateViewController = "showDateViewController"
    static var showCategoriesViewController = "showCategoriesViewController"
    static var showMainViewController = "showMainViewController"
}


// MARK: - Cell identifiers


struct CellId {
    static var listInfoAreaCell = "listInfoAreaCell"
    static var categoryCell = "categoryCell"
    static var settingsCell = "settingsCell"
}


// MARK: - Notifications


struct Notifications {
    static var showInputViewController = NSNotification.Name("showInputViewController")
    static var inputOptionPressed = NSNotification.Name("inputOptionPressed")
    static var itemsChanged = NSNotification.Name("itemsChanged")
    static var toggleDownMenu = NSNotification.Name("toggleDownMenu")
    static var isDownMenuActiveStatusChanged = NSNotification.Name("isDownMenuActiveStatusChanged")
    static var moneyBalanceChanged = NSNotification.Name("moneyBalanceChanged")
}

// MARK: - Images

struct ApplicationImages {
    static var addIncome = UIImage(named: "addIncome") ?? UIImage()
    static var addExpense = UIImage(named: "addExpense") ?? UIImage()
    static var categoryFood = UIImage(named: "food") ?? UIImage()
    static var categoryShopping = UIImage(named: "shopping") ?? UIImage()
    static var categoryCar = UIImage(named: "car") ?? UIImage()
    static var categoryPayments = UIImage(named: "payments") ?? UIImage()
    static var categoryLeisure = UIImage(named: "leisure") ?? UIImage()
    static var categoryOther = UIImage(named: "other") ?? UIImage()
    static var changeCurrency = UIImage(named: "currency") ?? UIImage()
    static var resetIncome = UIImage(named: "income") ?? UIImage()
    static var resetExpense = UIImage(named: "expense") ?? UIImage()
    static var resetAll = UIImage(named: "resetAll") ?? UIImage()
    static var shareApplication = UIImage(named: "share") ?? UIImage()
}


// MARK: - Input VC special keyboard buttons ids


struct KeyboardIds {
    static var dot = 10
    static var clear = 11
    static var ok = 12
    static var clearAll = 13
}


// MARK: - Input VC option buttons keys


struct InputOptionButtonIds {
    static var note = 1
    static var date = 2
    static var category = 3
}


// MARK: - Input VC main types


struct InputType {
    static var income = 0
    static var expense = 1
}


// MARK: - Item class types


class ItemType: NSObject, NSCoding {
    var income = 0
    var expense = 1
    var unknown = 2
    
    override init() { }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(income, forKey: "income")
        aCoder.encode(expense, forKey: "expense")
        aCoder.encode(unknown, forKey: "unknown")
    }
    
    required init?(coder aDecoder: NSCoder) {
        income = aDecoder.decodeObject(forKey: "income") as! Int
        expense = aDecoder.decodeObject(forKey: "expense") as! Int
        unknown = aDecoder.decodeObject(forKey: "unknown") as! Int
    }
   
}




