//
//  ApplicationStrings.swift
//  Spenderly
//
//  Created by Tino Krzelj on 26/05/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import Foundation

struct ApplicationStrings {
    static var mainNavigationBarAppNameTitle = NSLocalizedString("mainNavigationBarAppNameTitle", comment: "")
    static var balance = NSLocalizedString("balance", comment: "").uppercased()
    static var income = NSLocalizedString("income", comment: "").uppercased()
    static var expense = NSLocalizedString("expense", comment: "").uppercased()
    static var addNew = NSLocalizedString("addNew", comment: "")
    static var note = NSLocalizedString("note", comment: "")
    static var date = NSLocalizedString("date", comment: "")
    static var category = NSLocalizedString("category", comment: "")
    static var categories = NSLocalizedString("categories", comment: "")
    static var ok = NSLocalizedString("ok", comment: "")
    static var settings = NSLocalizedString("settings", comment: "")
    static var currency = NSLocalizedString("currency", comment: "")
    static var passcodeLock = NSLocalizedString("passcodeLock", comment: "")
    static var data = NSLocalizedString("data", comment: "")
    static var resetAllData = NSLocalizedString("resetAllData", comment: "")
    static var selectCategory = NSLocalizedString("selectCategory", comment: "")
    static var addYourNote = NSLocalizedString("addYourNote", comment: "")
    static var selectDate = NSLocalizedString("selectDate", comment: "")
    static var done = NSLocalizedString("done", comment: "")
    static var noItems = NSLocalizedString("noItems", comment: "")
    static var noNote = NSLocalizedString("noNote", comment: "")
    static var firstLaunchInfoMessage = NSLocalizedString("firstLaunchInfoMessage", comment: "")
    static var noCurrency = NSLocalizedString("noCurrency", comment: "")
    static var options = NSLocalizedString("settings", comment: "")
    static var resetAll = NSLocalizedString("resetAll", comment: "")
    static var resetIncomeValue = NSLocalizedString("resetIncome", comment: "")
    static var resetExpenseValue = NSLocalizedString("resetExpense", comment: "")
    static var changeCurrency = NSLocalizedString("changeCurrency", comment: "")
    static var shareApplications = NSLocalizedString("shareApplication", comment: "")
    static var shareMessage = NSLocalizedString("shareMessage", comment: "")
    
        // CATEGORY
    
    static var categoryFood = NSLocalizedString("categoryFood", comment: "")
    static var categoryShopping = NSLocalizedString("categoryShopping", comment: "")
    static var categoryCar = NSLocalizedString("categoryCar", comment: "")
    static var categoryPayments = NSLocalizedString("categoryPayments", comment: "")
    static var categoryLeisure = NSLocalizedString("categoryLeisure", comment: "")
    static var categoryOther = NSLocalizedString("categoryOther", comment: "")
    
}
