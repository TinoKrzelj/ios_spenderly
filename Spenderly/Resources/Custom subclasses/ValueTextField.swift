//
//  ValueTextField.swift
//  Spenderly
//
//  Created by Tino Krželj on 08/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class ValueTextField: UITextField {

    override func caretRect(for position: UITextPosition) -> CGRect {
        return CGRect.zero
    }

}
