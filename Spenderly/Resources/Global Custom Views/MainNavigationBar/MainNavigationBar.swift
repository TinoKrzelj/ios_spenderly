//
//  MainNavigationBar.swift
//  Spenderly
//
//  Created by Tino Krzelj on 26/05/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit
import MarqueeLabel

protocol MainNavigationBarDelegate {
    func showSideBarMenuButtonPressed()
    func cancelButtonPressed()
}

class MainNavigationBar: UIView {

    
    // MARK: - Properties
    
    
    var delegate: MainNavigationBarDelegate?
    
    
    // MARK: - IBOutlet
    
    
    @IBOutlet private weak var mainView: UIView!
    @IBOutlet private weak var appTitle: UILabel!
    @IBOutlet private weak var balanceLabel: MarqueeLabel!
    @IBOutlet private weak var cancelButton: UIButton!
    
    
    // MARK: - IBAction
    
    
    @IBAction private func showSideBarMenuButtonPressed() {
        delegate?.showSideBarMenuButtonPressed()
    }
    
    @IBAction private func cancelButtonPressed() {
        delegate?.cancelButtonPressed()
    }
    
    
    // MARK: - Inits
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        UINib(nibName: NibKey.mainNavigationBar, bundle: nil).instantiate(withOwner: self, options: nil)
        
        insertSubview(mainView, at: 0)
        mainView.frame = bounds
        
        configureMainNavigationBar()
    }
    
    
    // MARK: - Custom methods
    
    
    func configureMainNavigationBar() {
        balanceLabel.font = UIFont(name: FontNames.avenirNextMedium, size: SharedData.shared.returnCorrectHeaderBalanceFontHeightForDevice())
        balanceLabel.addMarqueLabelConfigurationToSelf()
        mainView.backgroundColor = ColorScheme.background
        appTitle.textColor = ColorScheme.whiteText
        appTitle.text = ApplicationStrings.mainNavigationBarAppNameTitle
    }
    
    func showBalanceLabelAndCancelButton(show: Bool) {
        balanceLabel.isHidden = !show
        cancelButton.isHidden = !show
    }
    
    func setCorrectValue(val: Double) -> String {
        var correctValue = ""
        
        let values = String(val).components(separatedBy: ".")
        if values[1] == "0" {
            correctValue = String(values[0])
        } else {
            correctValue = "\(values[0]).\(values[1])"
        }
        
        return correctValue
    }
        
    func updateBalanceLabel(amount: Double, currency: String) {
        if SharedData.shared.currencCurrency.isoName != ApplicationStrings.noCurrency {
            balanceLabel.text = "\(ApplicationStrings.balance): \(setCorrectValue(val: amount)) \(currency)"
        } else {
            balanceLabel.text = "\(ApplicationStrings.balance): \(setCorrectValue(val: amount))"
        }
    }

}
