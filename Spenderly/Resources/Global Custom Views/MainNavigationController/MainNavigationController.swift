//
//  MainNavigationController.swift
//  Spenderly
//
//  Created by Tino Krželj on 10/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {

    // MARK: - PROPERTIES
    
    private var isMenuActive = false {
        didSet {
            NotificationCenter.default.post(name: Notifications.isDownMenuActiveStatusChanged, object: isMenuActive)
        }
    }
    
    private var downMenuHeight: CGFloat = UIScreen.main.bounds.height / 1.6
    private var blockView: UIView!
    
    // MARK: - IBOUTLETS
    
    @IBOutlet private weak var downMenuContainerView: UIView!
    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var headerTitle: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    
    
    // MARK: - IBACTIONS
    
    // MARK: - CUSTOM METHODS
    
    private func setDefaultValues() {
        tableView.delegate = self
        tableView.dataSource = self
        let cellNib = UINib(nibName: NibKey.settingsCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: CellId.settingsCell)
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(MainNavigationController.toggleDownMenu), name: Notifications.toggleDownMenu, object: nil)
    }
    
    private func configureDownMenuContainer() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MainNavigationController.toggleDownMenu))
        tapGesture.numberOfTapsRequired = 1
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(MainNavigationController.toggleDownMenu))
        swipeGesture.direction = .up
        blockView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        blockView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        blockView.tag = 1000
        blockView.addGestureRecognizer(tapGesture)
        blockView.addGestureRecognizer(swipeGesture)
        setDownMenuYCoordinate(y: -downMenuHeight)
        hideBlockView()
        downMenuContainerView.roundBottomCorners(width: 20.0, height: 20.0)
        view.addSubview(downMenuContainerView)
    }
    
    private func setDownMenuYCoordinate(y: CGFloat) {
        downMenuContainerView.frame = CGRect(x: 0, y: y, width: UIScreen.main.bounds.width, height: downMenuHeight)
        //downMenuContainerView.isHidden = false
    }
    
    private func hideBlockView() {
        UIView.animate(withDuration: 0.6, animations: {
            self.blockView.alpha = 0.0
        }) { (finished) in
            for subv in self.view.subviews where subv.tag == 1000 {
                subv.removeFromSuperview()
            }
        }
    }
    
    private func showBlockView() {
        view.insertSubview(blockView, at: view.subviews.count - 2)
        
        UIView.animate(withDuration: 0.6) {
            self.blockView.alpha = 1.0
        }
    }
    
    private func hideMenu(completition: (() -> ())? = nil) {
        isMenuActive = false
        
        UIView.animate(withDuration: 0.4) {
            self.setDownMenuYCoordinate(y: -self.downMenuHeight)
            self.view.layoutIfNeeded()
        }
        hideBlockView()
        
        if completition != nil {
            completition!()
        }
    }
    
    private func showMenu() {
        if downMenuContainerView.isHidden { downMenuContainerView.isHidden = false }
        isMenuActive = true
        
        UIView.animate(withDuration: 1.4, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: .curveEaseInOut, animations: {
            self.setDownMenuYCoordinate(y: 0.0)
            self.view.layoutIfNeeded()
        }, completion: nil)
        showBlockView()
    }
    
    private func setHeaderValues() {
        headerView.backgroundColor = ColorScheme.background
        headerTitle.textColor = ColorScheme.whiteText
        headerTitle.text = ApplicationStrings.settings
    }
    
    // MARK: - SELECTOR METHODS
    
    @objc func toggleDownMenu() {
        if isMenuActive {
            hideMenu()
        } else {
            showMenu()
        }
    }
    
    // MARK: - VIEW METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForNotifications()
        setDefaultValues()
        configureDownMenuContainer()
        setHeaderValues()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - EXTENSION - UITableViewDelegate, UITableViewDataSource

extension MainNavigationController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SharedData.shared.availableSettingsOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CellId.settingsCell, for: indexPath) as? SettingCell {
            let option = SharedData.shared.availableSettingsOptions[indexPath.row]
            cell.configureCell(option: option)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.height / 5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedOption = SharedData.shared.availableSettingsOptions[indexPath.row]
        hideMenu {
            self.handleSelection(option: selectedOption)
        }
    }
    
    func handleSelection(option: SettingsOption) {
        switch option.id {
        case "0": changeCurrency()
        case "1": ItemService.shared.resetIncomeValue()
        case "2": ItemService.shared.resetExpenseValue()
        case "3": ItemService.shared.resetAllData()
        case "4": shareApplication()
        default:
            #if DEBUG
                print("MainNavigationController - Error - Unknown option selected.")
            #endif
        }
    }
    
    private func shareApplication() {
        let shareActivity = UIActivityViewController(activityItems: [ApplicationStrings.shareMessage], applicationActivities: [])
        present(shareActivity, animated: true, completion: nil)
    }
    
    private func changeCurrency() {
        if let currencyVC = storyboard?.instantiateViewController(withIdentifier: StoryboardIds.currencyViewController) as? CurrencyViewController {
            present(currencyVC, animated: true, completion: nil)
        }
    }
}
