//
//  SettingCell.swift
//  Spenderly
//
//  Created by Tino Krželj on 10/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {
    
    // MARK: - IBOUTLETS
    
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var optionsImage: UIImageView!
    @IBOutlet private weak var title: UILabel!
    
    // MARK: - CUSTOM METHODS
    
    private func configureShadow() {
        shadowView.setShadowWithColor(shadowColor: UIColor.gray)
        shadowView.setRadiusAndBackgroundColor(radius: 10.0, color: UIColor.white)
        containerView.setRadiusAndBackgroundColor(radius: 10.0, color: UIColor.white)
        containerView.layer.masksToBounds = true
    }
    
    private func setDefaultValues() {
        title.textColor = ColorScheme.darkGrey
    }
    
    func setSelections(selected: Bool) {
        if selected {
            containerView.backgroundColor = ColorScheme.backgroundLightBlack
        } else {
            containerView.backgroundColor = UIColor.white
        }
    }
    
    func configureCell(option: SettingsOption) {
        optionsImage.image = option.image
        title.text = option.name
    }
    
    // MARK: - VIEW METHODS
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureShadow()
    }

}
