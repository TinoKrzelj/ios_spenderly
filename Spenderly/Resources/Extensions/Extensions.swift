//
//  Extensions.swift
//  Spenderly
//
//  Created by Tino Krzelj on 17/06/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit
import MarqueeLabel

// MARK: - UITextField

extension UITextField {
    func setTextColorForState(inputType: Int) {
        switch inputType {
        case InputType.income: self.textColor = ColorScheme.incomeGreen
        case InputType.expense: self.textColor = ColorScheme.expenseRed
        default:
            #if DEBUG
                print("ERROR: - DetailsArea: Unknown input type.")
            #endif
            
        }
    }
}

// MARK: - UIImageView

extension UIImageView {
    func setCorrectSaveValueImage(inputType: Int) {
        switch inputType {
        case InputType.income: self.image = ApplicationImages.addIncome
        case InputType.expense: self.image = ApplicationImages.addExpense
        default:
            #if DEBUG
                print("ERROR: - DetailsArea: Unknown input type.")
            #endif
            
        }
    }
}

// MARK: - UILabel

extension UILabel {
    func setText(value: Double) {
        let values = String(value).components(separatedBy: ".")
        
        if values[1] == "0" {
            self.text = String(values[0])
        } else {
            let number = Double("\(values[0]).\(values[1])") ?? 0.0
            self.text = String(format: "%.2f", number)
        }
    }
}

// MARK: - UIButton

    // - Using setRadiusAndBackgroundColor from UIView's extension

// MARK: - UIView

extension UIView {
    func setRadiusAndBackgroundColor(radius: CGFloat, color: UIColor) {
        self.backgroundColor = color
        self.layer.cornerRadius = radius
    }
    
    func setShadowWithColor(shadowColor: UIColor) {
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.zero
    }
    
    func addBlurView() {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurContainer = UIVisualEffectView(effect: blurEffect)
        blurContainer.frame = self.bounds
        self.insertSubview(blurContainer, at: 0)
    }
    
    func roundBottomCorners(width: CGFloat, height: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: width, height: height))
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        self.layer.mask = shape
    }
    
}

//  MARK: - Date

extension Date {
    func getStringDate(withFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = withFormat
        return dateFormatter.string(from: self)
    }
    
}

// EXTENSION - MarqueLabel

extension MarqueeLabel {
    func addMarqueLabelConfigurationToSelf() {
        self.scrollDuration = 2.0
        self.marqueeType = .MLLeftRight
        self.triggerScrollStart()
    }
}
