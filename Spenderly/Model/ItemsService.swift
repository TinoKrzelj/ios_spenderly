//
//  ItemsService.swift
//  Spenderly
//
//  Created by Tino Krželj on 08/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import Foundation

class ItemService {

    static let shared = ItemService()
    private init() { }
    
    // MARK: - METHODS
    
    func addItem(item: Item) {
        if item.itemDate == nil { item.itemDate = Date() }
        if item.itemCategory == nil { item.itemCategory = SharedData.shared.availableCategories.last }
        
        switch item.type {
        case ItemType().income: SharedData.shared.currentIncomeBalance += Double(item.itemValue) ?? 0.0
        case ItemType().expense: SharedData.shared.currentExpenseBalance += Double(item.itemValue) ?? 0.0
        default:
            #if DEBUG
                print("ItemsService - Error: - Unknown item type.")
            #endif
        }
        
        SharedData.shared.items.append(item)
        SharedData.shared.currentOverallBalance = calculateOverallBalance()
    }
    
    func calculateOverallBalance() -> Double {
        var income = 0.0
        var expense = 0.0
        
        for item in SharedData.shared.items {
            if item.type == ItemType().income {
                income += Double(item.itemValue) ?? 0.0
            } else {
                expense += Double(item.itemValue) ?? 0.0
            }
        }
        
        return income - expense
    }
    
    func removeItem(item: Item) {
        if let index = SharedData.shared.items.index(where: { (searchItem: Item) -> Bool in
            return item.id == searchItem.id
        }) {
            switch item.type {
            case ItemType().income:
                if SharedData.shared.currentIncomeBalance > 0.0 {
                    SharedData.shared.currentIncomeBalance -= Double(item.itemValue) ?? 0.0
                }
            case ItemType().expense:
                if SharedData.shared.currentExpenseBalance > 0.0 {
                    SharedData.shared.currentExpenseBalance -= Double(item.itemValue) ?? 0.0
                }
            default:
                #if DEBUG
                    print("ItemsService - Error: - Unknown item type.")
                #endif
            }
            
            SharedData.shared.items.remove(at: index)
            SharedData.shared.currentOverallBalance = calculateOverallBalance()
        }
    }
    
    func resetIncomeValue() {
        SharedData.shared.currentIncomeBalance = 0.0
    }
    
    func resetExpenseValue() {
        SharedData.shared.currentExpenseBalance = 0.0
    }
    
    func resetOverallBalance() {
        SharedData.shared.currentOverallBalance = 0.0
    }
    
    func resetAllData() {
        SharedData.shared.items.removeAll()
        resetIncomeValue()
        resetExpenseValue()
        resetOverallBalance()
    }
    
}
