//
//  SharedData.swift
//  Spenderly
//
//  Created by Tino Krželj on 06/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import Foundation

class SharedData {
    
    // MARK: - SINGELTON PATTERN
    
    static let shared = SharedData()
    private init() {
        loadCustomCategories()
        loadStaticCurrencies()
        loadCurrentCurrency()
        loadSettingsOptions()
    }
    
    // MARK: - PROPERTIES
    
    var userDefaults = UserDefaults.standard
    var isApplicationStartedForTheFirstTime = true
    
    var currencCurrency: Currency! {
        didSet {
            let data = NSKeyedArchiver.archivedData(withRootObject: currencCurrency)
            userDefaults.set(data, forKey: UserDefaultsKeys.currentCurrency)
            userDefaults.synchronize()
        }
    }
    
    lazy var allCurrencies = [Currency]()
    
    var currentOverallBalance = 0.0 {
        didSet {
            userDefaults.set(currentOverallBalance, forKey: UserDefaultsKeys.currentOverallBalance)
            userDefaults.synchronize()
            NotificationCenter.default.post(name: Notifications.moneyBalanceChanged, object: nil)
        }
    }
    
    var currentIncomeBalance = 0.0 {
        didSet {
            userDefaults.set(currentIncomeBalance, forKey: UserDefaultsKeys.currentIncomeValue)
            userDefaults.synchronize()
           NotificationCenter.default.post(name: Notifications.moneyBalanceChanged, object: nil)
        }
    }
    
    var currentExpenseBalance = 0.0 {
        didSet {
            userDefaults.set(currentExpenseBalance, forKey: UserDefaultsKeys.currentExpenseValue)
            userDefaults.synchronize()
            NotificationCenter.default.post(name: Notifications.moneyBalanceChanged, object: nil)
        }
    }
    
    lazy var currentItem = Item()
    
    var items = [Item]() {
        didSet {
            let data = NSKeyedArchiver.archivedData(withRootObject: items)
            userDefaults.set(data, forKey: UserDefaultsKeys.items)
            userDefaults.synchronize()
            NotificationCenter.default.post(name: Notifications.itemsChanged, object: nil)
        }
    }
    
    lazy var availableCategories = [Category]()
    lazy var availableSettingsOptions = [SettingsOption]()
    
    // MARK: - CUSTOM METHODS
    
    private func loadCustomCategories() {
        availableCategories.append(Category(id: "0", name: ApplicationStrings.categoryFood,
                                            imageName: ApplicationImages.categoryFood))
        availableCategories.append(Category(id: "1", name: ApplicationStrings.categoryShopping,
                                            imageName: ApplicationImages.categoryShopping))
        availableCategories.append(Category(id: "2", name: ApplicationStrings.categoryCar,
                                            imageName: ApplicationImages.categoryCar))
        availableCategories.append(Category(id: "3", name: ApplicationStrings.categoryPayments,
                                            imageName: ApplicationImages.categoryPayments))
        availableCategories.append(Category(id: "4", name: ApplicationStrings.categoryLeisure,
                                            imageName: ApplicationImages.categoryLeisure))
        availableCategories.append(Category(id: "5", name: ApplicationStrings.categoryOther,
                                            imageName: ApplicationImages.categoryOther))
    }
    
    func returnCorrectHeaderBalanceFontHeightForDevice() -> CGFloat {
        var fontSize: CGFloat = 15.0
        
        switch Display.typeIsLike {
        case .iphoneSE: fontSize = 11.0
        case .iphone8: fontSize = 12.0
        case .iphone8plus: fontSize = 12.0
        case .iphoneX: fontSize = 12.0
        default:
            #if DEBUG
                print("Display: - Error - Unknown device")
            #endif
        }
        
        return fontSize
    }
    
    func returnCorrectTitleValueForBalanceFontSize() -> (title: CGFloat, value: CGFloat) {
        var title: CGFloat = 8.0
        var value: CGFloat = 12.0
        
        switch Display.typeIsLike {
        case .iphoneSE:
            title = 9.0
            value = 22.0
        case .iphone8:
            title = 9.0
            value = 25.0
        case .iphone8plus:
            title = 10.0
            value = 28.0
        case .iphoneX:
            title = 10.0
            value = 28.0
        default:
            #if DEBUG
                print("Display: - Error - Unknown device")
            #endif
        }
        
        return (title, value)
    }
    
    func loadStaticCurrencies() {
        for (isoName, name) in currenciesDict {
            allCurrencies.append(Currency(name: name, isoName: isoName))
        }
        
        allCurrencies.sort { (c1: Currency, c2: Currency) -> Bool in
            return c1.isoName < c2.isoName
        }
        
        allCurrencies.insert(Currency(name: "", isoName: ApplicationStrings.noCurrency), at: 0)
    }
    
    private func loadCurrentCurrency() {
        if let data = userDefaults.object(forKey: UserDefaultsKeys.currentCurrency) as? Data {
            if let currency = NSKeyedUnarchiver.unarchiveObject(with: data) as? Currency {
                currencCurrency = currency
            } else {
                currencCurrency = Currency()
            }
        } else {
            currencCurrency = Currency()
        }
    }
    
    private func loadSettingsOptions() {
        availableSettingsOptions.append(SettingsOption(id: "0", name: ApplicationStrings.changeCurrency, image: ApplicationImages.changeCurrency))
        availableSettingsOptions.append(SettingsOption(id: "1", name: ApplicationStrings.resetIncomeValue, image: ApplicationImages.resetIncome))
        availableSettingsOptions.append(SettingsOption(id: "2", name: ApplicationStrings.resetExpenseValue, image: ApplicationImages.resetExpense))
        availableSettingsOptions.append(SettingsOption(id: "3", name: ApplicationStrings.resetAll, image: ApplicationImages.resetAll))
        availableSettingsOptions.append(SettingsOption(id: "4", name: ApplicationStrings.shareApplications, image: ApplicationImages.shareApplication))
    }
}
