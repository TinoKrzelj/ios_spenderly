//
//  Item.swift
//  Spenderly
//
//  Created by Tino Krželj on 06/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import Foundation

class Item: NSObject, NSCoding {
    
    // MARK: - PROPERTIES
    
    private var _id: TimeInterval!
    private var _type: Int!
    private var _itemValue: String!
    private var _itemNote: String?
    private var _itemDate: Date?
    private var _itemCategory: Category?
    
    var id: TimeInterval {
        get { return _id ?? TimeInterval() }
        set { _id = newValue }
    }
    
    var type: Int {
        get { return _type ?? ItemType().unknown }
        set { _type = newValue }
    }
    
    var itemValue: String {
        get { return _itemValue ?? "0" }
        set { _itemValue = newValue }
    }
    
    var itemNote: String? {
        get { return _itemNote }
        set { _itemNote = newValue }
    }
    
    var itemDate: Date? {
        get { return _itemDate }
        set { _itemDate = newValue }
    }
    
    var itemCategory: Category? {
        get { return _itemCategory }
        set { _itemCategory = newValue }
    }
    
    // MARK: - INITS
    
    override init() { }
    
    // MARK: - NSCoding
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_id, forKey: "id")
        aCoder.encode(_type, forKey: "type")
        aCoder.encode(_itemValue, forKey: "itemValue")
        aCoder.encode(_itemNote, forKey: "itemNote")
        aCoder.encode(_itemDate, forKey: "itemDate")
        aCoder.encode(_itemCategory, forKey: "itemCategory")
    }
    
    required init?(coder aDecoder: NSCoder) {
        _id = aDecoder.decodeObject(forKey: "id") as! TimeInterval
        _type = aDecoder.decodeObject(forKey: "type") as! Int
        _itemValue = aDecoder.decodeObject(forKey: "itemValue") as! String
        _itemNote = aDecoder.decodeObject(forKey: "itemNote") as? String
        _itemDate = aDecoder.decodeObject(forKey: "itemDate") as? Date
        _itemCategory = aDecoder.decodeObject(forKey: "itemCategory") as? Category
    }
    
}

