//
//  Category.swift
//  Spenderly
//
//  Created by Tino Krželj on 07/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import Foundation

class Category: NSObject, NSCoding {
    
    // MARK: - PROPERTIES
    
    private var _id: String!
    private var _name: String!
    private var _imageName: UIImage!
    
    var id: String {
        get { return _id ?? "-1" }
        set { _id = newValue }
    }
    
    var name: String {
        get { return _name ?? "nil" }
        set { _name = newValue }
    }
    
    var image: UIImage {
        get { return _imageName ?? UIImage() }
        set { _imageName = newValue }
    }
    
    // MARK: - INITS
    
    override init() { }
    
    init(id: String, name: String, imageName: UIImage) {
        super.init()
        self.id = id
        self.name = name
        self.image = imageName
    }
    
    // MARK: - NSCoding
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_id, forKey: "id")
        aCoder.encode(_name, forKey: "name")
        aCoder.encode(_imageName, forKey: "image")
    }
    
    required init?(coder aDecoder: NSCoder) {
        _id = aDecoder.decodeObject(forKey: "id") as! String
        _name = aDecoder.decodeObject(forKey: "name") as! String
        _imageName = aDecoder.decodeObject(forKey: "image") as! UIImage
        
    }
    
}
