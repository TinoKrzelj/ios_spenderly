//
//  SettingsOption.swift
//  Spenderly
//
//  Created by Tino Krželj on 10/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class SettingsOption {
    
    // MARK: - PROPERTIES
    
    private var _id: String!
    private var _name: String!
    private var _image: UIImage!
    
    var id: String {
        get { return _id ?? "0" }
        set { _id = newValue }
    }
    
    var name: String {
        get { return _name ?? "nil" }
        set { _name = newValue }
    }
    
    var image: UIImage {
        get { return _image ?? UIImage() }
        set { _image = newValue }
    }
    
    // MARK: - INITS
    
    init() {
        self.id = ""
        self.name = ""
        self.image = UIImage()
    }
    
    init(id: String, name: String, image: UIImage) {
        self.id = id
        self.name = name
        self.image = image
    }
    
}
