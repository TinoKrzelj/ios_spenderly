//
//  Currency.swift
//  Spenderly
//
//  Created by Tino Krželj on 09/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import Foundation

class Currency: NSObject, NSCoding {
    
    // MARK: - PROPERTIES
    
    private var _name: String!
    private var _isoName: String!
    
    var name: String {
        get { return _name ?? "nil" }
        set { _name = newValue }
    }
    
    var isoName: String {
        get { return _isoName ?? "nil" }
        set { _isoName = newValue }
    }
    
    // MARK: - INIT METHODS
    
    override init() {
        _name = ""
        _isoName = ""
    }
    
    init(name: String, isoName: String) {
        _name = name
        _isoName = isoName
    }
    
    // MARK: - NSCoding
    
    required init?(coder aDecoder: NSCoder) {
        _name = aDecoder.decodeObject(forKey: "name") as! String
        _isoName = aDecoder.decodeObject(forKey: "isoName") as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_name, forKey: "name")
        aCoder.encode(_isoName, forKey: "isoName")
    }
    
}

