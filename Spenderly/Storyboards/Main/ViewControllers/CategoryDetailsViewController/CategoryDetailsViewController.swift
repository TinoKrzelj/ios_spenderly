//
//  CategoryDetailsViewController.swift
//  Spenderly
//
//  Created by Tino Krzelj on 17/06/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class CategoryDetailsViewController: UIViewController, MainNavigationBarDelegate {

    
    // MARK: - Properties

    
    private var selectedIndex = -1

    
    // MARK: - IBOutlets
    
    
    @IBOutlet private weak var navigationBar: MainNavigationBar!
    @IBOutlet private weak var navigationBarHeight: NSLayoutConstraint!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var selectButton: UIButton!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    // MARK: - IBActions
    
    @IBAction private func selectButtonPressed() {
        if selectedIndex != -1 {
            SharedData.shared.currentItem.itemCategory = SharedData.shared.availableCategories[selectedIndex]
        } else {
            SharedData.shared.currentItem.itemCategory = nil
        }
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Custom methods

    private func configureNavigationBarAndDefaultValues() {
        navigationBar.delegate = self
        navigationBar.showBalanceLabelAndCancelButton(show: true)
        setNavigationBarHeight()
        titleLabel.text = ApplicationStrings.selectCategory
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.clear
        let categoryCell = UINib(nibName: NibKey.categoryCell, bundle: nil)
        collectionView.register(categoryCell, forCellWithReuseIdentifier: CellId.categoryCell)
    }
    
    private func configureSelectButton() {
        selectButton.setRadiusAndBackgroundColor(radius: 10.0, color: ColorScheme.background)
        selectButton.setTitle(ApplicationStrings.done.uppercased(), for: .normal)
        selectButton.setTitleColor(ColorScheme.whiteText, for: .normal)
    }
    
    private func setNavigationBarHeight() {
        switch Display.typeIsLike {
        case .iphoneX: navigationBarHeight.constant = 120.0
        default:
            navigationBarHeight.constant = 80.0
        }
    }
    
    private func updateNavigationBarBalance() {
        navigationBar.updateBalanceLabel(amount: SharedData.shared.currentOverallBalance, currency: SharedData.shared.currencCurrency.isoName)
    }
    
    private func configureDoneButton(enable: Bool) {
        selectButton.isEnabled = enable
        
        UIView.animate(withDuration: 0.4) {
            if enable {
                self.selectButton.alpha = 1.0
            } else {
                self.selectButton.alpha = 0.4
            }
        }
    }
    
    private func checkIsItemAlreadySelected() {
        if SharedData.shared.currentItem.itemCategory != nil {
            if let stringId = SharedData.shared.currentItem.itemCategory?.id {
                if stringId != "-1" {
                    selectedIndex = Int(stringId) ?? -1
                } else {
                    selectedIndex = -1
                }
            }
            
            
            collectionView.reloadData()
        }
    }
    
    // MARK: - View methods
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarBalance()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBarAndDefaultValues()
        configureSelectButton()
        checkIsItemAlreadySelected()
    }
}


// MARK: - Extensions


extension CategoryDetailsViewController {

    
    // MARK: - MainNavigationBarDelegate
    
    
    func showSideBarMenuButtonPressed() {
        NotificationCenter.default.post(name: Notifications.toggleDownMenu, object: nil)
    }
    
    func cancelButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - EXTENSION - UICollectionView

extension CategoryDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SharedData.shared.availableCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellId.categoryCell, for: indexPath) as? CategoryCell {
            let category = SharedData.shared.availableCategories[indexPath.row]
            cell.configureCell(category: category)
            
            if indexPath.row == selectedIndex {
                cell.setSelection(isSelected: true)
            } else {
                cell.setSelection(isSelected: false)
            }
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width / 2,
                      height: collectionView.bounds.height / 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedIndex != indexPath.row {
            selectedIndex = indexPath.row
        } else {
            selectedIndex = -1
        }
        collectionView.reloadData()
    }
    
}
