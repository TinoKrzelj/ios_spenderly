//
//  CategoryCell.swift
//  Spenderly
//
//  Created by Tino Krželj on 07/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {

    // MARK: - IBOUTLETS
    
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var categoryImage: UIImageView!
    @IBOutlet private weak var categoryName: UILabel!
    
    // MARK: - CUSTOM METHODS

    func setDefaultValues() {
        shadowView.setRadiusAndBackgroundColor(radius: 8.0, color: UIColor.white)
        shadowView.setShadowWithColor(shadowColor: UIColor.gray)
        containerView.setRadiusAndBackgroundColor(radius: 8.0, color: UIColor.white)
        containerView.layer.masksToBounds = true
    }
    
    func configureCell(category: Category) {
        categoryImage.image = category.image
        categoryName.text = category.name
    }
    
    func setSelection(isSelected: Bool) {
        if isSelected {
            containerView.backgroundColor = UIColor.black.withAlphaComponent(0.08)
        } else {
            containerView.backgroundColor = UIColor.white
        }
    }
    
    // MARK: - VIEW METHODS
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDefaultValues()
    }

}
