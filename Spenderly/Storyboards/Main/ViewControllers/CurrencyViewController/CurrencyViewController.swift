//
//  CurrencyViewController.swift
//  Spenderly
//
//  Created by Tino Krželj on 09/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class CurrencyViewController: UIViewController {

    // MARK: - PROPERTIES
    
    // MARK: - IBOUTLETS
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var doneButton: UIButton!
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var currencyPicker: UIPickerView!
    
    @IBOutlet private weak var doneButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet private weak var textViewUpperConstraint: NSLayoutConstraint!
    
    // MARK: - IBACTIONS
    
    @IBAction private func doneButtonPressed() {
        SharedData.shared.currencCurrency = SharedData.shared.allCurrencies[currencyPicker.selectedRow(inComponent: 0)]
        performSegue(withIdentifier: CustomSegues.showMainViewController, sender: nil)
    }
    
    // MARK: - CUSTOM METHODS
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .default }
    
    private func setDefaultValues() {
        currencyPicker.delegate = self
        currencyPicker.dataSource = self
        doneButton.isEnabled = false
        doneButton.alpha = 0.4
        doneButtonBottomConstraint.constant = -100
        textViewUpperConstraint.constant = -100
        shadowView.alpha = 0.0
        containerView.alpha = 0.0
        doneButton.setRadiusAndBackgroundColor(radius: 10.0, color: ColorScheme.background)
        doneButton.setTitle(ApplicationStrings.done.uppercased(), for: .normal)
        doneButton.setTitleColor(ColorScheme.whiteText, for: .normal)
        textView.textColor = ColorScheme.darkGrey
        shadowView.setRadiusAndBackgroundColor(radius: 10.0, color: UIColor.white)
        containerView.setRadiusAndBackgroundColor(radius: 10.0, color: UIColor.white)
        shadowView.setShadowWithColor(shadowColor: UIColor.gray)
        containerView.layer.masksToBounds = true
        setInfoMessage(message: ApplicationStrings.firstLaunchInfoMessage)
    }
    
    internal func setInfoMessage(message: String) {
        textView.text = message
    }
    
    private func configureViewController() {
        view.backgroundColor = ColorScheme.backgroundGrey
    }
    
    private func animateTextViewAndDoneButton() {
        UIView.animate(withDuration: 2.0, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            self.doneButtonBottomConstraint.constant = 16
            self.textViewUpperConstraint.constant = 36
            self.view.layoutIfNeeded()
        }, completion: { (finished) in
            self.animateShadowAndContainerView()
        })
    }
    
    private func animateShadowAndContainerView() {
        UIView.animate(withDuration: 2.0, delay: 0.0, options: .curveEaseInOut, animations: {
            self.shadowView.alpha = 1.0
            self.containerView.alpha = 1.0
        }, completion: { (finished) in
            self.configureDoneButton(show: true)
        })
    }
    
    private func configureDoneButton(show: Bool) {
        doneButton.isEnabled = show
        
        UIView.animate(withDuration: 2.0) {
            if show {
                self.doneButton.alpha = 1.0
            } else {
                self.doneButton.alpha = 0.4
            }
        }
    }
    
    // MARK: - VIEW METHODS
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        animateTextViewAndDoneButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDefaultValues()
        configureViewController()
    }
}

// MARK: - UIPickerViewDataSource, UIPickerViewDelegate

extension CurrencyViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return SharedData.shared.allCurrencies.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return SharedData.shared.allCurrencies[row].isoName
    }
}
