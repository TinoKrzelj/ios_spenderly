//
//  MainInfoArea.swift
//  Spenderly
//
//  Created by Tino Krzelj on 26/05/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit
import MarqueeLabel

class MainInfoArea: UIView {

    
    // MARK: - IBOutlets
    
    
    @IBOutlet private weak var mainView: UIView!
    @IBOutlet private weak var balanceTitle: UILabel!
    @IBOutlet private weak var incomeTitle: UILabel!
    @IBOutlet private weak var expenseTitle: UILabel!
    @IBOutlet private weak var balanceValue: MarqueeLabel!
    @IBOutlet private weak var incomeValue: MarqueeLabel!
    @IBOutlet private weak var expenseValue: MarqueeLabel!
    
    
    // MARK: - Inits
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        UINib(nibName: NibKey.mainInfoArea, bundle: nil).instantiate(withOwner: self, options: nil)
        
        insertSubview(mainView, at: 0)
        mainView.frame = bounds
        
        configureMainInfoArea()
    }
    
    
    // MARK: - Custom methods
    
    
    func configureMainInfoArea() {
        mainView.backgroundColor = ColorScheme.background
        balanceTitle.font = UIFont(name: FontNames.avenirNextMedium, size: SharedData.shared.returnCorrectTitleValueForBalanceFontSize().title)
        incomeTitle.font = UIFont(name: FontNames.avenirNextMedium, size: SharedData.shared.returnCorrectTitleValueForBalanceFontSize().title)
        expenseTitle.font = UIFont(name: FontNames.avenirNextMedium, size: SharedData.shared.returnCorrectTitleValueForBalanceFontSize().title)
        balanceValue.font = UIFont(name: FontNames.avenirNextRegular, size: SharedData.shared.returnCorrectTitleValueForBalanceFontSize().value)
        incomeValue.font = UIFont(name: FontNames.avenirNextRegular, size: SharedData.shared.returnCorrectTitleValueForBalanceFontSize().value)
        expenseValue.font = UIFont(name: FontNames.avenirNextRegular, size: SharedData.shared.returnCorrectTitleValueForBalanceFontSize().value)
        
        
        balanceTitle.text = ApplicationStrings.balance
        incomeTitle.text = ApplicationStrings.income
        expenseTitle.text = ApplicationStrings.expense
        balanceValue.addMarqueLabelConfigurationToSelf()
        incomeValue.addMarqueLabelConfigurationToSelf()
        expenseValue.addMarqueLabelConfigurationToSelf()
    }
    
    func updateValues(newBalance: Double?, newIncome: Double?, newExpense: Double?) {
        if let balance = newBalance {
            balanceValue.setText(value: balance)
            if SharedData.shared.currencCurrency.isoName != ApplicationStrings.noCurrency {
                balanceValue.text! += " \(SharedData.shared.currencCurrency.isoName)"
            }
        }
        if let income = newIncome { incomeValue.setText(value: income) }
        if let expense = newExpense { expenseValue.setText(value: expense) }
    }
    
}
