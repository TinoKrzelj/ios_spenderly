//
//  ListInfoArea.swift
//  Spenderly
//
//  Created by Tino Krzelj on 26/05/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class ListInfoArea: UIView, UITableViewDataSource, UITableViewDelegate {
    
    
    // MARK: - IBOutlets
    
    
    @IBOutlet private weak var mainView: UIView!
    @IBOutlet private weak var inputsTableView: UITableView!
    @IBOutlet private weak var addNewButton: UIButton!
    @IBOutlet private weak var addNewButtonBottomConstraint: NSLayoutConstraint!
    

    // MARK: - IBActions
    
    
    @IBAction private func addNewButtonPressed() {
        NotificationCenter.default.post(name: Notifications.showInputViewController, object: nil)
    }
    
    // MARK: - Inits
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        UINib(nibName: NibKey.listInfoArea, bundle: nil).instantiate(withOwner: self, options: nil)
        
        insertSubview(mainView, at: 0)
        mainView.frame = bounds
        
        configureListInfoArea()
        configureInputsTableView()
    }
    
    
    // MARK: - Custom methods
    
    
    func configureListInfoArea() {
        mainView.backgroundColor = ColorScheme.backgroundGrey
        inputsTableView.backgroundColor = ColorScheme.backgroundLightBlack
        addNewButton.setTitleColor(ColorScheme.whiteText, for: .normal)
        addNewButton.setTitle(ApplicationStrings.addNew, for: .normal)
        setAddNewButtonBootomConstraint()
        animateAddNewButton(show: true, speed: 2.0)
    }
    
    func setAddNewButtonBootomConstraint() {
        switch Display.typeIsLike {
        case .iphoneX: addNewButtonBottomConstraint.constant = 30.0
        default: addNewButtonBottomConstraint.constant = 15.0
        }
    }
    
    func configureInputsTableView() {
        inputsTableView.delegate = self
        inputsTableView.dataSource = self
        inputsTableView.tableFooterView = UIView()
        
        let cellNib = UINib(nibName: NibKey.listInfoCell, bundle: nil)
        inputsTableView.register(cellNib, forCellReuseIdentifier: CellId.listInfoAreaCell)
    }
    
    func refreshTableView() {
        inputsTableView.reloadData()
    }
    
    func animateAddNewButton(show: Bool, speed: TimeInterval) {
        if show {
            switch Display.typeIsLike {
            case .iphoneX: self.addNewButtonBottomConstraint.constant = 30.0
            default: self.addNewButtonBottomConstraint.constant = 15.0
            }
        } else {
            self.addNewButtonBottomConstraint.constant = -100
        }
    }
}


// MARK - Extension


extension ListInfoArea {

    
    // MARK: - UITableViewDataSource, UITableViewDelegate
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SharedData.shared.items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.height / 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let inputCell = tableView.dequeueReusableCell(withIdentifier: CellId.listInfoAreaCell, for: indexPath) as? ListInfoCell {
            let item = SharedData.shared.items[indexPath.row]
            inputCell.configureInputCell(newItem: item)
            return inputCell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let itemToDelete = SharedData.shared.items[indexPath.row]
            ItemService.shared.removeItem(item: itemToDelete)
        }
    }
    
}


