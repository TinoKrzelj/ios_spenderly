//
//  ListInfoCell.swift
//  Spenderly
//
//  Created by Tino Krzelj on 27/05/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit
import MarqueeLabel

class ListInfoCell: UITableViewCell {

    
    // MARK: - IBOutlets
    
    
    @IBOutlet weak var noteLabel: MarqueeLabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: MarqueeLabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var cellSeparator: UIView!
    
    
    // MARK: - Custom methods
    
    
    func configureInputCell(newItem: Item) {
        noteLabel.text = newItem.itemNote ?? ApplicationStrings.noNote
        dateLabel.text = newItem.itemDate!.getStringDate(withFormat: preferedDateFormat)
        categoryImageView.image = newItem.itemCategory!.image
        amountLabel.setText(value: Double(newItem.itemValue) ?? 0.0)
        setCellAmountColor(item: newItem)
    }
        
    private func setCellAmountColor(item: Item) {
        if item.type == ItemType().income {
            amountLabel.textColor = ColorScheme.incomeGreen
        } else {
            amountLabel.textColor = ColorScheme.expenseRed
        }
    }
    
    private func setDefaultCellState() {
        amountLabel.addMarqueLabelConfigurationToSelf()
        noteLabel.addMarqueLabelConfigurationToSelf()
        backgroundColor = UIColor.white
        cellSeparator.backgroundColor = ColorScheme.separatorGrey
    }
    
    
    // MARK: - View methods
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setDefaultCellState()
    }
    
}
