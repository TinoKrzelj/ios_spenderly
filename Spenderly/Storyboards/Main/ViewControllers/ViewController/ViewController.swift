//
//  ViewController.swift
//  Spenderly
//
//  Created by Tino Krzelj on 26/05/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class ViewController: UIViewController, MainNavigationBarDelegate {

    
    // MARK: - IBOutlet 
    
    
    @IBOutlet private weak var navigationBar: MainNavigationBar!
    @IBOutlet weak var navigationBarHeight: NSLayoutConstraint!
    @IBOutlet private weak var mainInfoArea: MainInfoArea!
    @IBOutlet private weak var listInfoArea: ListInfoArea!
    @IBOutlet private weak var noItemsAlertContainer: UIView!
    @IBOutlet private weak var noItemLabel: UILabel!
    
    @IBOutlet weak var mainInfoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var listInfoAreaBottomConstraint: NSLayoutConstraint!
    
    // MARK - Custom methods
    
    
    func configureViewController() {
        navigationBar.delegate = self
        noItemLabel.textColor = ColorScheme.alertGrey
        noItemLabel.text = ApplicationStrings.noItems
        setNavigationBarHeight()
        SharedData.shared.items.isEmpty ? configureNoItemAlert(show: true) : configureNoItemAlert(show: false)
    }
    
    private func setNavigationBarHeight() {
        switch Display.typeIsLike {
        case .iphoneX:
            navigationBarHeight.constant = 120.0
            view.backgroundColor = UIColor.white
        default:
            navigationBarHeight.constant = 80.0
        }
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.showInputViewController), name: Notifications.showInputViewController, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.itemsChanged), name: Notifications.itemsChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.moneyBalanceChanged), name: Notifications.moneyBalanceChanged, object: nil)
    }
    
    func configureNoItemAlert(show: Bool) {
        UIView.animate(withDuration: 1.0) {
            if show {
                self.noItemsAlertContainer.alpha = 1.0
            } else {
                self.noItemsAlertContainer.alpha = 0.0
            }
        }
    }
    
    
    // MARK: - Selector methods
    
    
    @objc func showInputViewController() {
        performSegue(withIdentifier: CustomSegues.showInputViewController, sender: nil)
    }
    
    @objc func itemsChanged() {
        SharedData.shared.items.isEmpty ? configureNoItemAlert(show: true) : configureNoItemAlert(show: false)
        listInfoArea.refreshTableView()
        mainInfoArea.updateValues(newBalance: ItemService.shared.calculateOverallBalance(), newIncome: SharedData.shared.currentIncomeBalance, newExpense: SharedData.shared.currentExpenseBalance)
    }
    
    @objc func moneyBalanceChanged() {
        mainInfoArea.updateValues(newBalance: SharedData.shared.currentOverallBalance, newIncome: SharedData.shared.currentIncomeBalance, newExpense: SharedData.shared.currentExpenseBalance)
    }
    
    
    // MARK: - View methods
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mainInfoArea.updateValues(newBalance: SharedData.shared.currentOverallBalance, newIncome: SharedData.shared.currentIncomeBalance, newExpense: SharedData.shared.currentExpenseBalance)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForNotifications()
        configureViewController()
        navigationBar.showBalanceLabelAndCancelButton(show: false)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}


// MARK: - Extension


extension ViewController {

    
    // MARK: - MainNavigationBarDelegate
    
    
    func showSideBarMenuButtonPressed() {
        NotificationCenter.default.post(name: Notifications.toggleDownMenu, object: nil)
    }
    
    func cancelButtonPressed() {
        
    }
}
