//
//  InputViewController.swift
//  Spenderly
//
//  Created by Tino Krzelj on 26/05/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class InputViewController: UIViewController, MainNavigationBarDelegate, CustomKeyboardDelegate {

    // MARK: - Properties
    
    private var isDetailsAreaBottomConstraintSetted = false
    
    // MARK: - IBOutlets
    
    
    @IBOutlet private weak var navigationBar: MainNavigationBar!
    @IBOutlet private weak var navigationBarHeight: NSLayoutConstraint!
    @IBOutlet private weak var keyboardArea: KeyboardArea!
    @IBOutlet private weak var detailsArea: DetailsArea!
    @IBOutlet private weak var detailsAreaBottomConstraint: NSLayoutConstraint!
    
    
    // MARK: - Custom methods
    
    
    func configureInputViewController() {
        navigationBar.delegate = self
        keyboardArea.delegate = self
        detailsArea.delegate = self
        
        setNavigationBarHeight()
        view.backgroundColor = ColorScheme.background
    }
    
    private func setNavigationBarHeight() {
        switch Display.typeIsLike {
        case .iphoneX:
            navigationBarHeight.constant = 120.0
            view.backgroundColor = ColorScheme.background
        default:
            navigationBarHeight.constant = 80.0
        }
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(InputViewController.inputOptionPressed), name: Notifications.inputOptionPressed, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InputViewController.keyboardShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InputViewController.downMenuStatusChanged), name: Notifications.isDownMenuActiveStatusChanged, object: nil)
    }
    
    private func updateNavigationBarBalanceLabel() {
        navigationBar.updateBalanceLabel(amount: SharedData.shared.currentOverallBalance, currency: SharedData.shared.currencCurrency.isoName)
    }
    
    private func updateCurrentInputMoneyLabel() {
        //detailsArea.updateMoneyValueLabel(value: 0, currency: "BAM")
    }
    
    func setDetailsArea() {
        detailsArea.setKeyboard(show: true, completion: nil)
    }
    
    private func checkIsCurrentItemUpdated() {
        detailsArea.updateOptionButtons()
    }
    
    private func resetCurrentItem() {
        SharedData.shared.currentItem = Item()
    }
    
    // MARK: - Selector methods
    
    
    @objc func inputOptionPressed(notification: NSNotification) {
        if let tag = notification.object as? Int {
            switch tag {
            case InputOptionButtonIds.note: performSegue(withIdentifier: CustomSegues.showNoteViewController, sender: nil)
            case InputOptionButtonIds.date: performSegue(withIdentifier: CustomSegues.showDateViewController, sender: nil)
            case InputOptionButtonIds.category: performSegue(withIdentifier: CustomSegues.showCategoriesViewController, sender: nil)
            default:
                #if DEBUG
                    print("ERROR: OptionButtonsArea - Unknown button pressed.")
                #endif
            }
        }
    }

    @objc func keyboardShown(notification: NSNotification) {
        if !isDetailsAreaBottomConstraintSetted {
            if let size = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let realSize = size.cgRectValue
                self.detailsAreaBottomConstraint.constant = realSize.height
            }
            
            isDetailsAreaBottomConstraintSetted = true
        }
    }
    
    @objc func saveButtonPressed() {
        
    }
    
    @objc func downMenuStatusChanged(notification: NSNotification) {
        if let object = notification.object as? Bool {
            if object {
                detailsArea.setKeyboard(show: false, completion: nil)
            } else {
                detailsArea.setKeyboard(show: true, completion: nil)
            }
        }
    }
    
 
    // MARK: - View methods
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setDetailsArea()
        registerForNotifications()
        updateNavigationBarBalanceLabel()
        updateCurrentInputMoneyLabel()
        checkIsCurrentItemUpdated()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureInputViewController()
        navigationBar.showBalanceLabelAndCancelButton(show: true)
    }
    
}


// MARK: - Extension


extension InputViewController {
    
    
    // MARK: - MainNavigationBarDelegate
    
    
    func showSideBarMenuButtonPressed() {
        NotificationCenter.default.post(name: Notifications.toggleDownMenu, object: nil)
    }
    
    func cancelButtonPressed() {
        detailsArea.setKeyboard(show: false) {
            self.navigationController?.popViewController(animated: true)
            self.resetCurrentItem()
        }
    }
    
    
    // MARK: - CustomKeyboardDelegate
    
    
    func keyboardButtonPressed(withTag: Int) {
        print("Button with tag \(withTag) pressed")
    }
    
}

// MARK: - EXTENSION - DetailsArea delegate

extension InputViewController: DetailsAreaDelegate {
    func saveButtonPressedAndSuccessful() {
        ItemService.shared.addItem(item: SharedData.shared.currentItem)
        cancelButtonPressed()
    }
}
