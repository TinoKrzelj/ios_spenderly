//
//  DetailsArea.swift
//  Spenderly
//
//  Created by Tino Krzelj on 04/06/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit
import NXTSegmentedControl

protocol DetailsAreaDelegate {
    func saveButtonPressedAndSuccessful()
}

class DetailsArea: UIView {
    
    // MARK: - Properties
    
    
    var delegate: DetailsAreaDelegate?
    private var VALUE_MAX_NUMBER = 9
    
    // MARK: - IBOutlets
    
    
    @IBOutlet private weak var mainView: UIView!
    @IBOutlet private weak var selectedSwitch: UIView!
    @IBOutlet private weak var optionButtonsContainer: OptionButtonsArea!
    @IBOutlet private weak var moneyValueTextField: UITextField!
    @IBOutlet private weak var saveValueContainer: UIView!
    @IBOutlet private weak var saveValueImage: UIImageView!
    

    // MARK: - Inits
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        UINib(nibName: NibKey.detailsArea, bundle: nil).instantiate(withOwner: self, options: nil)
        
        insertSubview(mainView, at: 0)
        
        mainView.frame = bounds
        
        configureDetailsArea()
        configureSelectedSwitch()
    }
    
    
    // MARK: - Custom methods
    
    
    private func configureDetailsArea() {
        backgroundColor = ColorScheme.backgroundGrey
        configureSaveValueContainer(show: false)
        setMainBackgroundColor(color: UIColor.white, withAnimation: false)
        selectedSwitch.backgroundColor = ColorScheme.clearColor
        updateMoneyBalanceLabelColor(inputType: InputType.income)
        moneyValueTextField.addTarget(self, action: #selector(DetailsArea.textFieldDidChangeText), for: UIControlEvents.editingChanged)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(DetailsArea.saveValuePressed))
        tapGesture.numberOfTapsRequired = 1
        saveValueContainer.addGestureRecognizer(tapGesture)
        setKeyboard(show: true, completion: nil)
    }
    
    private func configureSelectedSwitch() {
        if let switchControll = NXTSegmentedControl(items: [ApplicationStrings.income.lowercased().capitalized,
                                                            ApplicationStrings.expense.lowercased().capitalized]) {
            switchControll.frame = selectedSwitch.bounds
            switchControll.tintColor = ColorScheme.background
            switchControll.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: ColorScheme.whiteText], for: .normal)
            switchControll.thumbColor = UIColor.white
            
            switchControll.addTarget(self, action: #selector(DetailsArea.inputTypeChanged), for: .valueChanged)
            
            selectedSwitch.addSubview(switchControll)
        }
    }
    
    private func updateMoneyBalanceLabelColor(inputType: Int) {
        if inputType == 0 {
            SharedData.shared.currentItem.type = ItemType().income
        } else {
            SharedData.shared.currentItem.type = ItemType().expense
        }
        
        moneyValueTextField.setTextColorForState(inputType: inputType)
        saveValueImage.setCorrectSaveValueImage(inputType: inputType)
    }
    
    func configureSaveValueContainer(show: Bool) {
        self.saveValueContainer.isHidden = !show
        UIView.animate(withDuration: 0.4) {
            if show {
                self.saveValueContainer.alpha = 1.0
            } else {
                self.saveValueContainer.alpha = 0.0
            }
        }
    }
    
    func setKeyboard(show: Bool, completion: (() ->())?) {
        if show {
            moneyValueTextField.becomeFirstResponder()
        } else {
            moneyValueTextField.resignFirstResponder()
        }
        
        if completion != nil {
            completion!()
        }
    }
    
    func updateOptionButtons() {
        optionButtonsContainer.updateOptionButtonsIfNeeded()
    }
    
    private func setMainBackgroundColor(color: UIColor, withAnimation: Bool) {
        if withAnimation {
            UIView.animate(withDuration: 1.0, animations: {
                self.mainView.backgroundColor = color
            })
        } else {
            mainView.backgroundColor = color
        }
    }
    
    
    // MARK: - Selector methods
    
    
    @objc func inputTypeChanged(_ sender: NXTSegmentedControl) {
        updateMoneyBalanceLabelColor(inputType: sender.selectedSegmentIndex)
        
    }
    
    @objc func saveValuePressed() {
        let textValue = moneyValueTextField.text ?? "*"
        
        if let doubleValue = Double(textValue) {
            SharedData.shared.currentItem.itemValue = String(doubleValue)
            SharedData.shared.currentItem.id = Date().timeIntervalSince1970
            delegate?.saveButtonPressedAndSuccessful()
        }
    
    }
}

// MARK: - EXTENSION - UITextField

extension DetailsArea: UITextFieldDelegate {
    @objc func textFieldDidChangeText() {
        if moneyValueTextField.text!.isEmpty {
            configureSaveValueContainer(show: false)
        } else {
            let textValue = moneyValueTextField.text ?? "*"
            if textValue.characters.count <= 9 {
                if mainView.backgroundColor == ColorScheme.alertRed.withAlphaComponent(0.5) {
                    setMainBackgroundColor(color: ColorScheme.whiteText, withAnimation: true)
                }
                if let doubleValue = Double(textValue), doubleValue > 0.0 {
                    if saveValueContainer.isHidden == true {
                        configureSaveValueContainer(show: true)
                    }
                } else {
                    if saveValueContainer.isHidden == false {
                        configureSaveValueContainer(show: false)
                    }
                }
            } else {
                setMainBackgroundColor(color: ColorScheme.alertRed.withAlphaComponent(0.5), withAnimation: true)
                if saveValueContainer.isHidden == false {
                    configureSaveValueContainer(show: false)
                }
            }
        }
    }
}
