//
//  OptionButtonsArea.swift
//  Spenderly
//
//  Created by Tino Krzelj on 04/06/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit
import AVFoundation
import MarqueeLabel

class OptionButtonsArea: UIView {

        
    // MARK: IBOutlets
    
    
    @IBOutlet private weak var mainView: UIView!
    @IBOutlet private weak var noteView: UIView!
    @IBOutlet private weak var dateView: UIView!
    @IBOutlet private weak var category: UIView!
    @IBOutlet private var separatorView: [UIView]!
    @IBOutlet private weak var noteLabel: MarqueeLabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var categoryLabel: UILabel!
    
    
    // MARK: - Inits
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        UINib(nibName: NibKey.optionButtonsArea, bundle: nil).instantiate(withOwner: self, options: nil)

        insertSubview(mainView, at: 0)
        mainView.frame = bounds
        
        configureOptionButtonsArea()
        addTargetToOptionViews()
    }
    
    
    // MARK: - Custom methods
   

    private func configureOptionButtonsArea() {
        backgroundColor = UIColor.clear
        noteLabel.addMarqueLabelConfigurationToSelf()
        mainView.backgroundColor = ColorScheme.clearColor
        noteView.backgroundColor = ColorScheme.clearColor
        dateView.backgroundColor = ColorScheme.clearColor
        category.backgroundColor = ColorScheme.clearColor
        
        noteLabel.text = ApplicationStrings.note
        dateLabel.text = ApplicationStrings.date
        categoryLabel.text = ApplicationStrings.category
        
        for view in separatorView { view.backgroundColor = ColorScheme.separatorGrey }
    }
    
    private func addTargetToOptionViews() {
        let noteTapGesture = UITapGestureRecognizer(target: self, action: #selector(OptionButtonsArea.noteOptionTapped))
        let dateTapGesture = UITapGestureRecognizer(target: self, action: #selector(OptionButtonsArea.dateOptionTapped))
        let categoryTapGesture = UITapGestureRecognizer(target: self, action: #selector(OptionButtonsArea.categoryOptionTapped))
        
        noteView.addGestureRecognizer(noteTapGesture)
        dateView.addGestureRecognizer(dateTapGesture)
        category.addGestureRecognizer(categoryTapGesture)
    }
    
    private func sendNotification(withTag: Int) {
        NotificationCenter.default.post(name: Notifications.inputOptionPressed, object: withTag)
    }
    
    func updateOptionButtonsIfNeeded() {
        let currentItem = SharedData.shared.currentItem
        
        if currentItem.itemNote != nil {
            noteLabel.text = currentItem.itemNote!
            noteLabel.textColor = ColorScheme.background
        } else {
            noteLabel.text = ApplicationStrings.note
            noteLabel.textColor = UIColor.lightGray
        }
        
        if currentItem.itemDate != nil {
            dateLabel.text = currentItem.itemDate!.getStringDate(withFormat: preferedDateFormat)
            dateLabel.textColor = ColorScheme.background
        } else {
            dateLabel.text = ApplicationStrings.date
            dateLabel.textColor = UIColor.lightGray
        }
        
        if currentItem.itemCategory != nil {
            categoryLabel.text = currentItem.itemCategory!.name
            categoryLabel.textColor = ColorScheme.background
        } else {
            categoryLabel.text = ApplicationStrings.category
            categoryLabel.textColor = UIColor.lightGray
        }
    }
    
    
    // MARK: - Selector methods
    
    
    @objc func noteOptionTapped() { sendNotification(withTag: 1) }
    @objc func dateOptionTapped() { sendNotification(withTag: 2) }
    @objc func categoryOptionTapped() { sendNotification(withTag: 3) }
    
}
