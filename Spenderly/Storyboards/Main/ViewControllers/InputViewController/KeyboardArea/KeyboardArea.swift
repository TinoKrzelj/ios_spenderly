//
//  KeyboardArea.swift
//  Spenderly
//
//  Created by Tino Krzelj on 03/06/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

protocol CustomKeyboardDelegate {
    func keyboardButtonPressed(withTag: Int)
}

class KeyboardArea: UIView {

    
    // MARK: - Properties
    
    
    var delegate: CustomKeyboardDelegate?
    
    
    // MARK: - IBOutlets
    
    
    @IBOutlet private weak var mainView: UIView!
    @IBOutlet private var keyboardButtons: [UIButton]!
    
    
    // MARK: - IBAction 
    
    
    @IBAction private func keyboardButtonPressed(_ sender: UIButton) {
        delegate?.keyboardButtonPressed(withTag: sender.tag)
    }
    
    
    // MARK: - Inits
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        UINib(nibName: NibKey.keyboardArea, bundle: nil).instantiate(withOwner: self, options: nil)
        
        insertSubview(mainView, at: 0)
        mainView.frame = bounds
        
        configureKeyboardArea()
        configureKeyboardLayout()
    }
    
    
    // MARK: - Custom methods
    
    
    private func configureKeyboardArea() {
        mainView.backgroundColor = ColorScheme.background
    }
    
    private func configureKeyboardLayout() {
        for key in keyboardButtons {
            key.setTitleColor(ColorScheme.whiteText, for: .normal)
            key.backgroundColor = ColorScheme.background
        }
    }
    
}
