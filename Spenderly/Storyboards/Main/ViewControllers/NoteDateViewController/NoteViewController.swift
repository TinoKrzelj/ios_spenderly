//
//  NoteDateViewController.swift
//  Spenderly
//
//  Created by Tino Krzelj on 17/06/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class NoteViewController: UIViewController, MainNavigationBarDelegate {
    
    // MARK: - Properties
    
    private var defaultSeelctButtonBottomConstraint: CGFloat = 16.0
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var navigationBar: MainNavigationBar!
    @IBOutlet private weak var navigationBarHeight: NSLayoutConstraint!
    @IBOutlet private weak var selectButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var noteTextField: UITextField!
    @IBOutlet private weak var selectButtonBottomConstraint: NSLayoutConstraint!
    
    
    // MARK: - IBActions
    
    
    @IBAction private func selectButtonPressed() {
        if SharedData.shared.currentItem.itemNote != nil {
            if noteTextField.text!.isEmpty {
                SharedData.shared.currentItem.itemNote = nil
            } else {
                SharedData.shared.currentItem.itemNote = noteTextField.text!
            }
        } else {
            if !noteTextField.text!.isEmpty {
                SharedData.shared.currentItem.itemNote = noteTextField.text!
            }
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Custom methods
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(NoteViewController.keyboardIsShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NoteViewController.keyboardIsHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NoteViewController.downMenuStatusChanged), name: Notifications.isDownMenuActiveStatusChanged, object: nil)
        
    }
    
    private func setDefaultValues() {
        noteTextField.delegate = self
        noteTextField.addTarget(self, action: #selector(NoteViewController.textFieldTextChanged), for: UIControlEvents.editingChanged)
        titleLabel.text = ApplicationStrings.addYourNote
        shadowView.setRadiusAndBackgroundColor(radius: 8.0, color: UIColor.white)
        shadowView.setShadowWithColor(shadowColor: UIColor.gray)
        containerView.setRadiusAndBackgroundColor(radius: 8.0, color: UIColor.white)
        containerView.layer.masksToBounds = true
        configureSelectButton()
    }
    
    private func configureNavigationBar() {
        navigationBar.delegate = self
        navigationBar.showBalanceLabelAndCancelButton(show: true)
        setNavigationBarHeight()
    }
    
    private func setNavigationBarHeight() {
        switch Display.typeIsLike {
        case .iphoneX: navigationBarHeight.constant = 120.0
        default:
            navigationBarHeight.constant = 80.0
        }
    }
    
    private func configureSelectButton() {
        selectButton.setRadiusAndBackgroundColor(radius: 10.0, color: ColorScheme.background)
        selectButton.setTitle(ApplicationStrings.done.uppercased(), for: .normal)
        selectButton.setTitleColor(ColorScheme.whiteText, for: .normal)
    }
    
    private func updateNavigationBarBalance() {
        navigationBar.updateBalanceLabel(amount: SharedData.shared.currentOverallBalance, currency: SharedData.shared.currencCurrency.isoName)
    }
    
    private func configureSelectButton(enabled: Bool) {
        selectButton.isEnabled = enabled
        
        UIView.animate(withDuration: 0.4) {
            if enabled {
                self.selectButton.alpha = 1.0
            } else {
                self.selectButton.alpha = 0.4
            }
        }
    }
    
    private func checkIsNoteAlreadySetted() {
        if SharedData.shared.currentItem.itemNote != nil {
            noteTextField.text = SharedData.shared.currentItem.itemNote!
        }
    }
    
    // MARK: - Selector methods
    
    @objc func keyboardIsShown(notification: NSNotification) {
        if let size = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let realSize = size.cgRectValue
            
            UIView.animate(withDuration: 2.0, animations: {
                self.selectButtonBottomConstraint.constant = (realSize.height + 8.0)
            })
        }
    }
    
    @objc func keyboardIsHidden() {
        UIView.animate(withDuration: 2.0) {
            self.selectButtonBottomConstraint.constant = self.defaultSeelctButtonBottomConstraint
        }
    }
    
    
    // MARK: - View methods
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        noteTextField.resignFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarBalance()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForNotifications()
        setDefaultValues()
        configureNavigationBar()
        checkIsNoteAlreadySetted()
        noteTextField.text!.isEmpty ? configureSelectButton(enabled: false) : configureSelectButton(enabled: true)
    }
    
}

// MARK: - Extensions


extension NoteViewController {
    
    
    // MARK: - MainNavigationBarDelegate
    
    func showSideBarMenuButtonPressed() {
        NotificationCenter.default.post(name: Notifications.toggleDownMenu, object: nil)
    }
    
    func cancelButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
    
}


// MARK: - EXTENSION - UITextFieldDelegate


extension NoteViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        noteTextField.resignFirstResponder()
        return true
    }
    
        // Selector method
    
    @objc func textFieldTextChanged() {
        if noteTextField.text != nil {
            if SharedData.shared.currentItem.itemNote == nil {
                noteTextField.text!.isEmpty ? configureSelectButton(enabled: false) : configureSelectButton(enabled: true)
            } else {
                if noteTextField.text!.isEmpty {
                    configureSelectButton(enabled: true)
                }
            }
        }
    }
    
    @objc func downMenuStatusChanged(notification: NSNotification) {
        if let object = notification.object as? Bool {
            if object {
                noteTextField.resignFirstResponder()
            } else {
                noteTextField.becomeFirstResponder()
            }
        }
    }

}
