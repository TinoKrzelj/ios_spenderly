//
//  DateViewController.swift
//  Spenderly
//
//  Created by Tino Krželj on 07/10/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

class DateViewController: UIViewController, MainNavigationBarDelegate {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var navigationBar: MainNavigationBar!
    @IBOutlet private weak var navigationBarHeight: NSLayoutConstraint!
    @IBOutlet private weak var selectButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var datePickerView: UIDatePicker!
    
    
    // MARK: - IBActions
    
    @IBAction private func selectButtonPressed() {
        SharedData.shared.currentItem.itemDate = datePickerView.date
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Custom methods
    
    private func setDefaultValues() {
        titleLabel.text = ApplicationStrings.selectDate
        configureSelectButton()
        shadowView.setRadiusAndBackgroundColor(radius: 8.0, color: UIColor.white)
        shadowView.setShadowWithColor(shadowColor: UIColor.gray)
        containerView.setRadiusAndBackgroundColor(radius: 8.0, color: UIColor.white)
        containerView.layer.masksToBounds = true
    }
    
    private func configureSelectButton() {
        selectButton.setRadiusAndBackgroundColor(radius: 10.0, color: ColorScheme.background)
        selectButton.setTitle(ApplicationStrings.done.uppercased(), for: .normal)
        selectButton.setTitleColor(ColorScheme.whiteText, for: .normal)
    }
    
    private func configureNavigationBar() {
        navigationBar.delegate = self
        navigationBar.showBalanceLabelAndCancelButton(show: true)
        setNavigationBarHeight()
    }
    
    private func setNavigationBarHeight() {
        switch Display.typeIsLike {
        case .iphoneX: navigationBarHeight.constant = 120.0
        default:
            navigationBarHeight.constant = 80.0
        }
    }
    
    private func updateNavigationBarBalance() {
        navigationBar.updateBalanceLabel(amount: SharedData.shared.currentOverallBalance, currency: SharedData.shared.currencCurrency.isoName)
    }
    
    private func checkIsItemAlreadySelected() {
        if SharedData.shared.currentItem.itemDate != nil {
            let date = SharedData.shared.currentItem.itemDate!
            datePickerView.setDate(date, animated: true)
        }
    }
    
    // MARK: - View methods
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarBalance()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        setDefaultValues()
        checkIsItemAlreadySelected()
    }

}

// MARK: - Extensions

extension DateViewController {
    
    
    // MARK: - MainNavigationBarDelegate
    
    func showSideBarMenuButtonPressed() {
        NotificationCenter.default.post(name: Notifications.toggleDownMenu, object: nil)
    }
    
    func cancelButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
    
}
