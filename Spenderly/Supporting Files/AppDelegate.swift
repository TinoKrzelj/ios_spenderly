//
//  AppDelegate.swift
//  Spenderly
//
//  Created by Tino Krzelj on 26/05/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit
import Firebase
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let value = SharedData.shared.userDefaults.object(forKey: UserDefaultsKeys.applicationFirstLaunch) as? Bool {
            SharedData.shared.isApplicationStartedForTheFirstTime = value
        }
        
        if let overallBalance = SharedData.shared.userDefaults.object(forKey: UserDefaultsKeys.currentOverallBalance) as? Double {
            SharedData.shared.currentOverallBalance = overallBalance
        }
        
        if let incomeBalance = SharedData.shared.userDefaults.object(forKey: UserDefaultsKeys.currentIncomeValue) as? Double {
            SharedData.shared.currentIncomeBalance = incomeBalance
        }
        
        if let expenseBalance = SharedData.shared.userDefaults.object(forKey: UserDefaultsKeys.currentExpenseValue) as? Double {
            SharedData.shared.currentExpenseBalance = expenseBalance
        }
        
        if let data = SharedData.shared.userDefaults.object(forKey: UserDefaultsKeys.items) as? Data {
            if let items = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Item] {
                SharedData.shared.items = items
            }
        }
        
        if SharedData.shared.isApplicationStartedForTheFirstTime {
            window?.rootViewController = storyboard.instantiateViewController(withIdentifier: StoryboardIds.currencyViewController) as! CurrencyViewController
            window?.makeKeyAndVisible()
            SharedData.shared.isApplicationStartedForTheFirstTime = false
            SharedData.shared.userDefaults.set(false, forKey: UserDefaultsKeys.applicationFirstLaunch)
            SharedData.shared.userDefaults.synchronize()
        } else {
            window?.rootViewController = storyboard.instantiateViewController(withIdentifier: StoryboardIds.mainNavigationController) as! MainNavigationController
            window?.makeKeyAndVisible()
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }

}

